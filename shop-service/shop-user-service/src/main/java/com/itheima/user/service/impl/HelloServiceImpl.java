package com.itheima.user.service.impl;

import com.itheima.api.IHelloService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;

/**
 * @author tcwgq
 * @since 2022/7/16 22:49
 */
@Service
@DubboService(interfaceClass = IHelloService.class)
public class HelloServiceImpl implements IHelloService {
    @Override
    public String hello(String name) {
        return name;
    }

}
