-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: 192.168.245.128    Database: trade
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `trade_coupon`
--

DROP TABLE IF EXISTS `trade_coupon`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trade_coupon`
(
    `coupon_id`    bigint    NOT NULL COMMENT '优惠券ID',
    `coupon_price` decimal(10, 2) DEFAULT NULL COMMENT '优惠券金额',
    `user_id`      bigint         DEFAULT NULL COMMENT '用户ID',
    `order_id`     bigint         DEFAULT NULL COMMENT '订单ID',
    `is_used`      int            DEFAULT NULL COMMENT '是否使用 0未使用 1已使用',
    `used_time`    timestamp NULL DEFAULT NULL COMMENT '使用时间',
    PRIMARY KEY (`coupon_id`),
    KEY `FK_trade_coupon` (`user_id`),
    KEY `FK_trade_coupon2` (`order_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trade_coupon`
--

LOCK TABLES `trade_coupon` WRITE;
/*!40000 ALTER TABLE `trade_coupon`
    DISABLE KEYS */;
INSERT INTO `trade_coupon`
VALUES (345988230098857984, 20.00, 345963634385633280, 746478722751795200, 1, '2022-07-17 21:41:02');
/*!40000 ALTER TABLE `trade_coupon`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trade_goods`
--

DROP TABLE IF EXISTS `trade_goods`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trade_goods`
(
    `goods_id`     bigint    NOT NULL AUTO_INCREMENT,
    `goods_name`   varchar(255)   DEFAULT NULL COMMENT '商品名称',
    `goods_number` int            DEFAULT NULL COMMENT '商品库存',
    `goods_price`  decimal(10, 2) DEFAULT NULL COMMENT '商品价格',
    `goods_desc`   varchar(255)   DEFAULT NULL COMMENT '商品描述',
    `add_time`     timestamp NULL DEFAULT NULL COMMENT '添加时间',
    PRIMARY KEY (`goods_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 345959443973935105
  DEFAULT CHARSET = utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trade_goods`
--

LOCK TABLES `trade_goods` WRITE;
/*!40000 ALTER TABLE `trade_goods`
    DISABLE KEYS */;
INSERT INTO `trade_goods`
VALUES (345959443973935104, '华为P30', 997, 1000.00, '夜间拍照更美', '2019-07-09 12:38:00');
/*!40000 ALTER TABLE `trade_goods`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trade_goods_number_log`
--

DROP TABLE IF EXISTS `trade_goods_number_log`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trade_goods_number_log`
(
    `goods_id`     bigint    NOT NULL COMMENT '商品ID',
    `order_id`     bigint    NOT NULL COMMENT '订单ID',
    `goods_number` int            DEFAULT NULL COMMENT '库存数量',
    `log_time`     timestamp NULL DEFAULT NULL,
    KEY `idx_order_id` (`order_id`) /*!80000 INVISIBLE */
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trade_goods_number_log`
--

LOCK TABLES `trade_goods_number_log` WRITE;
/*!40000 ALTER TABLE `trade_goods_number_log`
    DISABLE KEYS */;
INSERT INTO `trade_goods_number_log`
VALUES (345959443973935104, 746476916818710528, -1, '2022-07-17 21:33:52'),
       (345959443973935104, 746476916818710528, 1, '2022-07-17 21:33:52'),
       (345959443973935104, 746477706375467008, -1, '2022-07-17 21:37:00'),
       (345959443973935104, 746477706375467008, 1, '2022-07-17 21:37:00'),
       (345959443973935104, 746478409810579456, -1, '2022-07-17 21:39:48'),
       (345959443973935104, 746478722751795200, -1, '2022-07-17 21:41:02');
/*!40000 ALTER TABLE `trade_goods_number_log`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trade_mq_consumer_log`
--

DROP TABLE IF EXISTS `trade_mq_consumer_log`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trade_mq_consumer_log`
(
    `msg_id`             varchar(50)       DEFAULT NULL,
    `group_name`         varchar(100) NOT NULL,
    `msg_tag`            varchar(100) NOT NULL,
    `msg_key`            varchar(100) NOT NULL,
    `msg_body`           varchar(500)      DEFAULT NULL,
    `consumer_status`    int               DEFAULT NULL COMMENT '0:正在处理;1:处理成功;2:处理失败',
    `consumer_times`     int               DEFAULT NULL,
    `consumer_timestamp` timestamp    NULL DEFAULT NULL,
    `remark`             varchar(500)      DEFAULT NULL,
    PRIMARY KEY (`group_name`, `msg_tag`, `msg_key`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trade_mq_consumer_log`
--

LOCK TABLES `trade_mq_consumer_log` WRITE;
/*!40000 ALTER TABLE `trade_mq_consumer_log`
    DISABLE KEYS */;
INSERT INTO `trade_mq_consumer_log`
VALUES ('7F0000015AB418B4AAC25B87E0C70004', 'order_orderTopic_cancel_group', 'order_cancel', '746476916818710528',
        '{\"couponId\":345988230098857984,\"goodsId\":345959443973935104,\"goodsNum\":1,\"orderId\":746476916818710528,\"userId\":345963634385633280,\"userMoney\":100}',
        1, 0, '2022-07-17 21:33:52', NULL),
       ('7F0000015AB418B4AAC25B8AC0160005', 'order_orderTopic_cancel_group', 'order_cancel', '746477706375467008',
        '{\"couponId\":345988230098857984,\"goodsId\":345959443973935104,\"goodsNum\":1,\"orderId\":746477706375467008,\"userId\":345963634385633280,\"userMoney\":100}',
        1, 0, '2022-07-17 21:37:00', NULL);
/*!40000 ALTER TABLE `trade_mq_consumer_log`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trade_mq_producer_temp`
--

DROP TABLE IF EXISTS `trade_mq_producer_temp`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trade_mq_producer_temp`
(
    `id`          varchar(100) NOT NULL,
    `group_name`  varchar(100)          DEFAULT NULL,
    `msg_topic`   varchar(100)          DEFAULT NULL,
    `msg_tag`     varchar(100)          DEFAULT NULL,
    `msg_key`     varchar(100)          DEFAULT NULL,
    `msg_body`    varchar(500)          DEFAULT NULL,
    `msg_status`  int                   DEFAULT NULL COMMENT '0:未处理;1:已经处理',
    `create_time` timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trade_mq_producer_temp`
--

LOCK TABLES `trade_mq_producer_temp` WRITE;
/*!40000 ALTER TABLE `trade_mq_producer_temp`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `trade_mq_producer_temp`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trade_order`
--

DROP TABLE IF EXISTS `trade_order`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trade_order`
(
    `order_id`        bigint    NOT NULL COMMENT '订单ID',
    `user_id`         bigint         DEFAULT NULL COMMENT '用户ID',
    `order_status`    int            DEFAULT NULL COMMENT '订单状态 0未确认 1已确认 2已取消 3无效 4退款',
    `pay_status`      int            DEFAULT NULL COMMENT '支付状态 0未支付 1支付中 2已支付',
    `shipping_status` int            DEFAULT NULL COMMENT '发货状态 0未发货 1已发货 2已收货',
    `address`         varchar(255)   DEFAULT NULL COMMENT '收货地址',
    `consignee`       varchar(255)   DEFAULT NULL COMMENT '收货人',
    `goods_id`        bigint         DEFAULT NULL COMMENT '商品ID',
    `goods_number`    int            DEFAULT NULL COMMENT '商品数量',
    `goods_price`     decimal(10, 2) DEFAULT NULL COMMENT '商品价格',
    `goods_amount`    decimal(10, 0) DEFAULT NULL COMMENT '商品总价',
    `shipping_fee`    decimal(10, 2) DEFAULT NULL COMMENT '运费',
    `order_amount`    decimal(10, 2) DEFAULT NULL COMMENT '订单价格',
    `coupon_id`       bigint         DEFAULT NULL COMMENT '优惠券ID',
    `coupon_paid`     decimal(10, 2) DEFAULT NULL COMMENT '优惠券',
    `money_paid`      decimal(10, 2) DEFAULT NULL COMMENT '已付金额',
    `pay_amount`      decimal(10, 2) DEFAULT NULL COMMENT '支付金额',
    `add_time`        timestamp NULL DEFAULT NULL COMMENT '创建时间',
    `confirm_time`    timestamp NULL DEFAULT NULL COMMENT '订单确认时间',
    `pay_time`        timestamp NULL DEFAULT NULL COMMENT '支付时间',
    PRIMARY KEY (`order_id`),
    KEY `FK_trade_order` (`user_id`),
    KEY `FK_trade_order2` (`goods_id`),
    KEY `FK_trade_order3` (`coupon_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trade_order`
--

LOCK TABLES `trade_order` WRITE;
/*!40000 ALTER TABLE `trade_order`
    DISABLE KEYS */;
INSERT INTO `trade_order`
VALUES (746476916818710528, 345963634385633280, 2, 2, NULL, '北京', NULL, 345959443973935104, 1, 1000.00, 1000, 0.00,
        1000.00, 345988230098857984, 20.00, 100.00, 880.00, '2022-07-17 21:33:51', NULL, NULL),
       (746477706375467008, 345963634385633280, 2, NULL, NULL, '北京', NULL, 345959443973935104, 1, 1000.00, 1000, 0.00,
        1000.00, 345988230098857984, 20.00, 100.00, 880.00, '2022-07-17 21:37:00', NULL, NULL),
       (746478409810579456, 345963634385633280, 1, 0, NULL, '北京', NULL, 345959443973935104, 1, 1000.00, 1000, 0.00,
        1000.00, 345988230098857984, 20.00, 100.00, 880.00, '2022-07-17 21:39:47', '2022-07-17 21:39:48', NULL),
       (746478722751795200, 345963634385633280, 1, 0, NULL, '北京', NULL, 345959443973935104, 1, 1000.00, 1000, 0.00,
        1000.00, 345988230098857984, 20.00, 100.00, 880.00, '2022-07-17 21:41:02', '2022-07-17 21:41:02', NULL);
/*!40000 ALTER TABLE `trade_order`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trade_pay`
--

DROP TABLE IF EXISTS `trade_pay`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trade_pay`
(
    `pay_id`     bigint NOT NULL COMMENT '支付编号',
    `order_id`   bigint         DEFAULT NULL COMMENT '订单编号',
    `pay_amount` decimal(10, 2) DEFAULT NULL COMMENT '支付金额',
    `is_paid`    int            DEFAULT NULL COMMENT '是否已支付 1否 2是',
    PRIMARY KEY (`pay_id`),
    KEY `FK_trade_pay` (`order_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trade_pay`
--

LOCK TABLES `trade_pay` WRITE;
/*!40000 ALTER TABLE `trade_pay`
    DISABLE KEYS */;
INSERT INTO `trade_pay`
VALUES (746477234566602752, 746476916818710528, 880.00, 0),
       (746477234566602753, 746476916818710528, 880.00, 2);
/*!40000 ALTER TABLE `trade_pay`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trade_user`
--

DROP TABLE IF EXISTS `trade_user`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trade_user`
(
    `user_id`       bigint    NOT NULL AUTO_INCREMENT COMMENT '用户ID',
    `user_name`     varchar(255)   DEFAULT NULL COMMENT '用户姓名',
    `user_password` varchar(255)   DEFAULT NULL COMMENT '用户密码',
    `user_mobile`   varchar(255)   DEFAULT NULL COMMENT '手机号',
    `user_score`    int            DEFAULT NULL COMMENT '积分',
    `user_reg_time` timestamp NULL DEFAULT NULL COMMENT '注册时间',
    `user_money`    decimal(10, 0) DEFAULT NULL COMMENT '用户余额',
    PRIMARY KEY (`user_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 345963634385633281
  DEFAULT CHARSET = utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trade_user`
--

LOCK TABLES `trade_user` WRITE;
/*!40000 ALTER TABLE `trade_user`
    DISABLE KEYS */;
INSERT INTO `trade_user`
VALUES (345963634385633280, '刘备', '123L', '18888888888L', 100, '2019-07-09 05:37:03', 800);
/*!40000 ALTER TABLE `trade_user`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trade_user_money_log`
--

DROP TABLE IF EXISTS `trade_user_money_log`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trade_user_money_log`
(
    `user_id`        bigint    NOT NULL COMMENT '用户ID',
    `order_id`       bigint    NOT NULL COMMENT '订单ID',
    `money_log_type` int       NOT NULL COMMENT '日志类型 1订单付款 2 订单退款',
    `use_money`      decimal(10, 2) DEFAULT NULL,
    `create_time`    timestamp NULL DEFAULT NULL COMMENT '日志时间',
    PRIMARY KEY (`user_id`, `order_id`, `money_log_type`),
    KEY `FK_trade_user_money_log2` (`order_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trade_user_money_log`
--

LOCK TABLES `trade_user_money_log` WRITE;
/*!40000 ALTER TABLE `trade_user_money_log`
    DISABLE KEYS */;
INSERT INTO `trade_user_money_log`
VALUES (345963634385633280, 746476916818710528, 1, 100.00, '2022-07-17 21:33:52'),
       (345963634385633280, 746476916818710528, 2, 100.00, '2022-07-17 21:33:52'),
       (345963634385633280, 746477706375467008, 1, 100.00, '2022-07-17 21:37:00'),
       (345963634385633280, 746477706375467008, 2, 100.00, '2022-07-17 21:37:00'),
       (345963634385633280, 746478409810579456, 1, 100.00, '2022-07-17 21:39:48'),
       (345963634385633280, 746478722751795200, 1, 100.00, '2022-07-17 21:41:02');
/*!40000 ALTER TABLE `trade_user_money_log`
    ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2022-07-20 12:09:21
