package com.itheima.api;

/**
 * @author tcwgq
 * @since 2022/7/16 22:48
 */
public interface IHelloService {
    String hello(String name);

}
