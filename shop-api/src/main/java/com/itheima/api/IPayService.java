package com.itheima.api;

import com.itheima.entity.Result;
import com.itheima.shop.pojo.TradePay;

public interface IPayService {

    Result createPayment(TradePay tradePay);

    Result callbackPayment(TradePay tradePay) throws Exception;

}
