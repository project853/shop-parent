package com.itheima.shop.controller;

import com.itheima.api.IHelloService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloController {
    @DubboReference
    private IHelloService IHelloService;

    @RequestMapping("/{name}")
    public String confirmOrder(@PathVariable String name) {
        return IHelloService.hello(name);
    }

}
